1. Run `yarn` (or `npm install`) to install all the dependencies
2. Run `yarn run dev` (or `npm run dev`) for the development mode

#Note: Maybe you will have a trouble with node-sass, therefore sass-loader will not working. You can fix it with: 
`yarn upgrade node-sass` or (`npm rebuild node-sass`)