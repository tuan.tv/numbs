import React, {Component} from 'react'
import PlayPage from '../Layout/PlayPage.jsx'

export default class PlayController extends Component {
    constructor(props) {
        super(props)

        this.state = {
            level: 1,
            timer: 0,
            counter: 3,
            score: 0,
            endcode: false,
            timeEndcode: 0,
            content: '12345',
            finished: false,
            wrong: 0
        }

        this.countDown = this.countDown.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.randomString = this.randomString.bind(this)
    }

     randomString(length, chars) {
        let result = ''

        for (let i = length; i > 0; --i)
            result += chars[Math.round(Math.random() * (chars.length - 1))]

        return result
    }

    countDown() {
        this.setState({
            timer: this.state.timer - 1,
            timeEndcode: this.state.timeEndcode - 1
        })

        if(this.state.timeEndcode <= 0)
            this.setState({endcode: true})
        else
            this.setState({endcode: false})

        if(this.state.timer <= 0)
            clearInterval(this.timer)
    }

    async handleSubmit(inputValue) {
        if(inputValue === this.state.content) {
            let newContent

            if(this.state.level >= 4)
                newContent = await this.randomString(this.state.level + 4, '0123456789asdw')
            else
                newContent = await this.randomString(this.state.level + 4, '0123456789')

            await this.setState({
                content: newContent,
                score: this.state.score + parseInt(this.state.timer / 5),
                counter: this.state.counter + 1,
                level: parseInt(this.state.counter / 3),
                timeEndcode: this.state.level >= 4 ? 40 : 20
            })

            await clearInterval(this.timer)

            await this.setState({timer: (this.state.level + 5) * 10})

            this.timer = setInterval(() => this.countDown(), 100)
        } else if(inputValue !== this.state.content && this.state.timer <= 0) {
            this.setState({
                score: this.state.score >= 5 ? this.state.score - 5 : this.state.score,
                wrong: this.state.wrong + 1,
                finished: true
            })
        } else if(inputValue !== this.state.content) {
            this.setState({
                score: this.state.score >= 5 ? this.state.score - 5 : this.state.score,
                wrong: this.state.wrong + 1
            })
        }
    }

    componentDidMount() {
        this.setState({
            timer: (this.state.level + 5) * 10,
            timeEndcode: this.state.level >= 4 ? 40 : 20
        })
        this.timer = setInterval(() => this.countDown(), 100)
    }

    componentWillUnmount() {
        clearInterval(this.timer);
    }

    render() {
        return (
            <PlayPage timer={this.state.timer} content={this.state.content} endcode={this.state.endcode}
            handleSubmit={this.handleSubmit} score={this.state.score} level={this.state.level}
            runGame={this.runGame} finished={this.state.finished} wrong={this.state.wrong} />
        )
    }
}
