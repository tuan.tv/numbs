import React, {Component} from 'react'
import FacebookLogin from 'react-facebook-login';
import axios from 'axios'
import { withRouter } from 'react-router-dom';

class PlayPage extends Component {
    constructor(props) {
        super(props)

        this.state = {
            inputText: '',
            isAuth: false,
            player: {},
            isSaving: false,
        }

        this.onChange = this.onChange.bind(this)
        this.submitAnswer = this.submitAnswer.bind(this)
        this.renderContent = this.renderContent.bind(this)
        this.responseFacebook = this.responseFacebook.bind(this)
        this.saveScore = this.saveScore.bind(this)
    }

    submitAnswer(event) {
        event.preventDefault()
        this.props.handleSubmit(this.state.inputText)
        this.setState({inputText: ''})
    }

    onChange(event) {
        this.setState({inputText: event.target.value})
    }

    renderContent() {
        var renderCont = ''

        if(!this.props.endcode)
            renderCont = this.props.content
        else
            for(let i = 0; i < this.props.content.length; i++)
                renderCont += '*'
        return renderCont
    }

    responseFacebook(response) {
        if (response && response.id) {
            const player = response
            this.setState({ isAuth: true, player })
        }
    }

    getListPlayer() {
        const url = 'https://alitebit.herokuapp.com/api/numbs'
        axios({
            method:'get',
            url,
        })
        .then((res) => {
            window.localStorage.setItem('numbs-players', JSON.stringify(res.data))
        })
    }

    saveScore() {
        if (!this.state.isAuth) {
            this.props.history.push('/')
        }

        let data = {}
        let method = 'post'
        let url = 'https://alitebit.herokuapp.com/api/numbs'

        this.setState({ isSaving: true })

        const email = this.state.player.email

        data = {
            name: this.state.player.name,
            fbId: this.state.player.id,
            email: this.state.player.email,
            image: this.state.player.picture.data.url,
            score: [this.props.score],
            lastUpdate: Date.now()
        }

        const playerList = JSON.parse(window.localStorage.getItem('numbs-players'))

        for (let i = 0; i < playerList.length; i++) {
            const item = playerList[i]

            if (item.email === email) {
                method = 'put'
                url += `/${item._id}`

                data = Object.assign({}, item, data, { score: [...item.score, this.props.score] })

                break
            }
        }

        axios({
            method,
            url,
            data,
        })
        .then((res) => {
            this.setState({ isSaving: false })
            this.props.history.push('/')
            this.getListPlayer()
        })
    }

    componentDidMount() {
        this.answer.focus()
    }

    render() {
        const styleF = {
            display: this.props.finished ? 'block' : 'none'
        }

        const styleP = {
            display: this.props.finished ? 'none' : 'block'
        }

        const styleFooter = {
            display: this.state.isAuth ? 'none' : 'block'
        }

        return (
            <div>
                <div className="play-page" style={styleP} >
                    <header>
                        <h2>Level {this.props.level}</h2>
                        <span className="score">Scores : {this.props.score}</span>
                        <span className="timer">Time remaining : {this.props.timer} s</span>
                    </header>

                    <main>
                        <h2>{this.renderContent()}</h2>
                    </main>

                    <form action="#" onSubmit={this.submitAnswer} >
                        <input type="text" className="form-control"
                        value={this.state.inputText}
                        ref={input => this.answer = input}
                        onChange={this.onChange} />
                    </form>
                </div>

                <div className="finish-page" style={styleF} >
                    <header>
                        <h2>Finished</h2>
                    </header>

                    <main>
                        <span className="level">Level {this.props.level}</span>
                        <span className="score">Scores : {this.props.score}</span>
                        <span className="wrong">Wrongs : {this.props.wrong}</span>
                    </main>

                    <footer style={styleFooter}>
                        <p className="note">Hey hey, wait a minute, login with Facebook to keep your score in ranking board.</p>
                        <FacebookLogin
                            appId="305414106644378"
                            autoLoad={true}
                            fields="name,email,picture"
                            onClick={this.componentClicked}
                            callback={this.responseFacebook} />
                    </footer>

                    <button onClick={this.saveScore} className="btn" >
                        { this.state.isAuth ? this.state.isSaving ? 'Saving...' : 'Save & Try again' : 'Try again' }
                    </button>
                </div>
            </div>
        )
    }
}

export default withRouter(PlayPage)
