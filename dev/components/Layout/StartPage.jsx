import React, {Component} from 'react'
import {Link} from 'react-router-dom'

export default class StartPage extends Component {
    render() {
        return (
            <div className="play-page">
                <header>
                    <h2>Level 1</h2>
                    <span className="score">Scores : 0</span>
                    <span className="timer">Time remaining : 120 s</span>
                </header>

                <main>
                    <h2>12345</h2>
                </main>

                <Link to="/play" className="btn">Start game</Link>
            </div>
        )
    }
}
