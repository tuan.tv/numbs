import React, {Component} from 'react'
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom'
import axios from 'axios'
import { orderBy } from 'lodash'

import StartPage from './StartPage.jsx'
import AboutPage from './AboutPage.jsx'
import AuthorPage from './AuthorPage.jsx'
import PlayController from '../Controller/PlayController.jsx'

import clone from '../../utils'

import createHistory from 'history/createBrowserHistory'
const history = createHistory()

export default class MainLayout extends Component {
    constructor() {
        super()
        this.state = {
            loading: true,
            players: []
        }
    }
    getListPlayer() {
        const url = 'https://alitebit.herokuapp.com/api/numbs'
        axios({
            method:'get',
            url,
        })
        .then((res) => {
            let players = clone(res.data)

            players.map((item) => {
                item.score = Math.max(...item.score)
                return item
            })

            players = orderBy(players, ['score'], ['desc'])

            this.setState({ players })
            window.localStorage.setItem('numbs-players', JSON.stringify(res.data))
        })
    }
    componentDidMount() {
        this.getListPlayer()
    }
    render() {
        return (
            <Router history={history}>
                <div className="container">
                    <header className="header">
                        <div className="logo">
                            <Link to="/">Numbs</Link>
                        </div>
                        <div className="nav">
                            <Link to="/about">About game</Link>
                            <Link to="/author">Author</Link>
                        </div>
                    </header>

                    <div className="user-ranking">
                        {
                            this.state.players.map((player, index) => {
                                if (index <= 4) {
                                    return (
                                        <div key={index} className="box-ranking">
                                            <img className="box-ranking__image" src={player.image} alt=""/>

                                            <div className="box-ranking__info">
                                                <p className="name">{player.name}</p>
                                                <p className="score">{player.score} points</p>
                                            </div>

                                            <span className="box-ranking__rank">{ index + 1 }</span>
                                        </div>
                                    )
                                }
                            })
                        }
                    </div>

                    <main className="main">
                        <div className="row">
                            <div className="col-md-6 offset-md-3">
                                <Route exact path="/" component={StartPage} />
                                <Route path="/play" locations={{pathname: '/play'}} component={PlayController} />
                                <Route path="/about" component={AboutPage} />
                                <Route path="/author" component={AuthorPage} />
                            </div>
                        </div>
                    </main>
                </div>
            </Router>
        )
    }
}
