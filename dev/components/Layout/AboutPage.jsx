import React, {Component} from 'react'
import {Link} from 'react-router-dom'

export default class AboutPage extends Component {
    render() {
        return (
            <div className="about">
                <main>
                    <p>About this game (In comming)</p>
                </main>
                <footer>
                    <Link to="/" className="btn" >Back to start</Link>
                </footer>
            </div>
        )
    }
}
