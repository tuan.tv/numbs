import React, {Component} from 'react'
import {Link} from 'react-router-dom'

export default class AuthorPage extends Component {
    render() {
        return (
            <div className="author">
                <main>
                    <a href="https://www.facebook.com/itachi.ama" target="_blank">
                        <i className="fab fa-facebook"></i>
                        Facebook
                    </a>

                    <a href="https://twitter.com/Itachi_Akat" target="_blank">
                        <i className="fab fa-twitter"></i>
                        Twitter
                    </a>
                </main>
                <footer>
                    <Link to="/" className="btn" >Back to start</Link>
                </footer>
            </div>
        )
    }
}
