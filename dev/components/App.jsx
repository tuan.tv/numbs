import React from 'react'
import MainLayout from './Layout/MainLayout.jsx'

export default class App extends React.Component {
    render() {
        return (
            <MainLayout/>
        )
    }
}
