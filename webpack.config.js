const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ExtractTextPlugin = require("extract-text-webpack-plugin")

const isProd = process.env.NODE_ENV === 'production'
const cssProd = ExtractTextPlugin.extract({
    fallback: 'style-loader',
    use: [{
        loader: 'css-loader', options: { sourceMap: true, minimize: true }
    },
    {
        loader: 'sass-loader'
    }],
    publicPath: '/assets'
})
const cssDev = ['style-loader', 'css-loader?sourceMap', 'sass-loader']
const cssConfig = isProd ? cssProd : cssDev

const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
    title: 'Numbs',
    template: './dev/index.ejs'
})

const ExtractTextPluginConfig = new ExtractTextPlugin({
    filename: 'css/[name].css',
    disable: !isProd,
    allChunks: true
})

module.exports = {
    devServer: {
        contentBase: path.join(__dirname, 'assets'),
        compress: true,
        hot: true,
        stats: 'errors-only',
        historyApiFallback: true,
        host: "fcc.tuan",
        port: 8080,
        https: true
    },
    entry: ['babel-polyfill', './dev/index.js'],
    output: {
        path: path.resolve(__dirname, 'assets'),
        filename: 'bundle.js'
    },
    devtool: 'source-map',
    module: {
        rules: [
            { test: /\.scss$/, use:  cssConfig },
            { test: /\.js$/, use: 'babel-loader', exclude: /node_modules/ },
            { test: /\.jsx$/, use: 'babel-loader', exclude: /node_modules/ }
        ]
    },
    plugins: [
        HtmlWebpackPluginConfig,
        ExtractTextPluginConfig,
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin()
    ]
}
